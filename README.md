# angular2-on-sails

a [Sails](http://sailsjs.com) 1.0 application serving an Angular 2 front end.

This is a modification of the [sails-webpack-seed](https://github.com/sailshq/sails-webpack-seed) repo that implements the [Angular2 seed app](https://github.com/angular/angular2-seed).

**The main modifications to `angular2-seed` are:**

* The Webpack config is altered to output the compiled files to `.tmp/public`, since this is where Sails looks for static files.
* The Webpack config includes the `CleanWebpack` and `CopyWebpack` plugins, which clean out the `.tmp/public` directory and copy over static files like images and fonts from the `assets` directory.

**The main modifications to `sails-webpack-seed` are:**

* Front-end app is stored in `/src` directory.
* Webpack config is stored in `/webpack.config.js`.
* The extract-text plugin is not used to extract CSS into a separate file, as this seems to cause problems with Angular's own .css implementation.  There's probably a way to make this work.
* Extra dependencies and `.json` files to facilitate running Angular2.

See the [README for `sails-webpack-seed`](https://github.com/sailshq/sails-webpack-seed#sails-webpack-seed) for more info.
